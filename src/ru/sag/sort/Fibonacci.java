package ru.sag.sort;

import java.util.Scanner;

/**
 * Класс наождения n-ого числа Фибоначчи
 *
 * @author Sumka Andrey 18it18
 */

public class Fibonacci {

    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число фибоначчи - ");
        int n = sc.nextInt();
        System.out.println("Числа фибоначчи - " + searchFibanacci(n));
        System.out.println("Число фибоначии рекурсией - " + reverseSearchFib(n));
    }

    /**
     * Метод нахождения числа Фибоначчи
     *
     * @param n неизвестное число, заданное в консоли
     * @return вывод числа
     */
    public static int searchFibanacci (int n) {
        int a = 0;
        int b = 1;
        for (int i = 2; i <= n; ++i) {
            int next = a + b;
            a = b;
            b = next;
        }
        time();
        return b;
    }

    /**
     * Метод нахождения числа Фибоначчи рекурсией
     *
     * @param n неизвестное число, заданное в консоли
     * @return вывод числа
     */
        public static int reverseSearchFib(int n) {
            if (n == 0) {
                return 0;
            } else if (n == 1) {
                return 1;
            } else {
                return reverseSearchFib(n - 1) + reverseSearchFib(n - 2);
            }
        }

    /**
     * Метод нахождения времени по unix
     */
    public static void time () {
            long before = System.currentTimeMillis();
            //тут что-то происходит, продолжительность которого хотим померить
            long after = System.currentTimeMillis();
            System.out.println(after - before + " миллисекунд");
        }
}

