package ru.sag.sort;

import java.util.Arrays;

/**
 * Сортировка массива пузырьковым методом
 *
 * @author Sumka Andrey 18it18
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] arr = {9, 8, 3, 6, 7, 5, 1, 2, 4};
        bubbleSort(arr);
    }

    private static void bubbleSort(int[] arr) {
        //Внешний цикл каждый раз сокращает фрагмент массива, так как внутренний цикл каждый раз ставит в конец фрагмента максимальный элемент
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                 //Сравниваем элементы попарно, если они имеют неправильный порядок, то меняем местами
                if (arr[j] > arr[j + 1]) {
                    int replace = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = replace;
                }
            }
        }
       System.out.println(Arrays.toString(arr));
    }
}

