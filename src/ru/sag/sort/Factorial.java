package ru.sag.sort;

import java.util.Scanner;

public class Factorial {

    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число эн факториала - ");
        long n = sc.nextInt();
        System.out.println("Факториал числа итеративным способом - " + factSort(n));
        System.out.println("Факториал числ рекурсивным способом - " + factRecurse(n));
    }

    public static int factSort(long n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }

    public static int factRecurse(long n) {
        if (n == 0 || n == 1) {
            return 1;
        }
        return (int) (factRecurse(n- 1) * n);
    }
}
