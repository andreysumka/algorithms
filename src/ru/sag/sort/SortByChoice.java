package ru.sag.sort;

import java.util.Arrays;

/**
 * Сортировка массива выборочным методом
 *
 * @author Sumka Andrey 18it18
 */

public class SortByChoice {
    public static void main(String[] args) {
        int[] array = {3,6,4,1,7,9,5,8,2};

        selectionSort(array);
    }
    public static void selectionSort(int[] array) {
        for (int number = 0; number < array.length; number++) {
            //Предполагаем, что первый элемент (в каждом подмножестве элементов) является минимальным
            int min = array[number];
            int minNum = number;
             //В оставшейся части подмножества ищем элемент, который меньше предположенного минимума
            for (int num = number + 1; num < array.length; num++) {
                if (array[num] < min) {
                    min = array[num];
                    minNum = num;
                }
            }
            //Если нашелся элемент, меньший, чем на текущей позиции, меняем их местами
                    int replace = array[number];
                    array[number] = array[minNum];
                    array[minNum] = replace;
        }
        System.out.println(Arrays.toString(array));
    }
}
